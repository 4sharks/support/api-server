import express ,{Express,Request,Response} from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import dotenv from 'dotenv';
import connectDB from './config/db.js'
import ticketRoutes from './routes/ticketRoutes.js'

async function startup(){
    dotenv.config();
    try{
        connectDB();
        const app: Express = express();
        const port = process.env.PORT || 8881;

        // Middlewares
        app.use(cors({origin: '*',}));
        app.use(express.json());
        app.use(express.urlencoded({ extended: true }));
        app.use(bodyParser.json());
        
        // Use Routes
        app.use('/support',ticketRoutes);
        
        app.listen(port,() => {
            console.log(`Now listening on port:${port}`);
        });
    }catch(err) {
        console.log(err);
    }
}

startup();