import mongoose from 'mongoose';

const connectDB = async () => {
    try{
        const connection = process.env.CONNECTION_STRING || 'mongodb://auth-mongodb-service:27017/support_db?directConnection=true&serverSelectionTimeoutMS=2000&appName=mongosh+1.10.6';
        const connect = await mongoose.connect(connection);
        console.log(
            'Database connection established',
            connect.connection.host,
            connect.connection.name
        );
    } catch(err){
        console.log(err);
        process.exit(1);
    }
}

export default connectDB;