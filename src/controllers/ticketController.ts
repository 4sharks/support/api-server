import Ticket from '../models/ticketModel.js';
import {Request,Response} from 'express';

type ticketType = {
    customerName : string,
    customerId : string,
    email : string,
    mobile : string,
    subject : string,
    content : string,
    sectionId   : string,
    ProductId: string,
    status: string,
    attachments: string,
    createdBy: String,
    tenantId:  String,
    companyId: String,
    branchId:  String
}

export const addTicket = async (req: Request,res: Response) => {
    const {
        customerName ,
        customerId,
        email,
        mobile,
        subject,
        content,
        sectionId,
        ProductId,
        status,
        attachments,
        createdBy,
        tenantId,
        companyId,
        branchId
    } = req.body as ticketType | any ;
    if(!customerName || !email || !mobile || !subject || !content ){
        
        res.status(500).json({
            status:0,
            message:'Invalid Customer All Fields Are Required'
        });
    }
    const _added = await Ticket.create({
        customerName,
        customerId,
        email,
        mobile,
        subject,
        content,
        sectionId,
        ProductId,
        status,
        attachments,
        createdBy,
        tenantId,
        companyId,
        branchId
    });

    if(_added){
        return res.status(200).json({
            status: 1,
            message: 'successfully added',
            data:_added
        });
    }

    return res.status(500).json({
        status: 0,
        message:'Error during processing add',
        data:_added
    });
    
};

export const getAllTickets = async (req: Request, res: Response) => {
    const limit: number = parseInt(req.params.limit) || 50;
    const _tickets = await Ticket.find({}).sort({createdAt:-1}).limit(limit);
    if(_tickets){
        return res.status(200).json({
            status: 1 ,
            message:'Success',
            data:_tickets
        });
    }
    return res.status(500).json({
        status: 0 ,
        message:'Error',
        data:_tickets
    });
}

export const getByTenant = async (req: Request, res: Response) => {
    const limit: number = parseInt(req.params.limit) || 50;
    const tenantId = req.params.tenantId
    const companyId = req.params.companyId
    const branchId = req.params.branchId

    const _tickets = await Ticket.find({
        tenantId: tenantId,
        companyId: companyId,
        branchId: branchId
    }).sort({createdAt:-1}).limit(limit);
    
    if(_tickets){
        return res.status(200).json({
            status: 1 ,
            message:'Success',
            data:_tickets
        });
    }
    return res.status(500).json({
        status: 0 ,
        message:'Error',
        data:_tickets
    });
}
export const getById = async (req: Request, res: Response) => {
    const id = req.params.id

    const _ticket = await Ticket.findById(id);
    
    if(_ticket){
        return res.status(200).json({
            status: 1 ,
            message:'Success',
            data:_ticket
        });
    }
    return res.status(500).json({
        status: 0 ,
        message:'Error',
        data:_ticket
    });
}

export const createDetails = async(req:Request, res:Response) => {
    const {
        ticketId,
        content,
        attachments,
        createdBy
    } = req.body;

    if(!createdBy || !content ){
        res.status(500).json({
            status:0,
            message:'Invalid ticket detaiils All Fields Are Required'
        });
    }

    const _updated = await Ticket.findByIdAndUpdate(
        ticketId,
        {
            $push:{
                details:{
                    content,
                    attachments,
                    createdBy,
                    createdAt: Date.now()
                }
            }
        }
    );

    if(_updated){
        return res.status(200).json({
            status: 1,
            message: 'successfully updated successfully',
            data:_updated
        });
    }

    return res.status(500).json({
        status: 0,
        message:'Error during processing updat',
        data:_updated
    });
}


export const deleteByTenant = async (req: Request, res: Response) => {
    const tenantId = req.params.tenantId
    const companyId = req.params.companyId
    const branchId = req.params.branchId

    const _tickets = await Ticket.deleteMany({
        tenantId: tenantId,
        companyId: companyId,
        branchId: branchId
    });
    
    if(_tickets){
        return res.status(200).json({
            status: 1 ,
            message:'Success',
            data:_tickets
        });
    }
    return res.status(500).json({
        status: 0 ,
        message:'Error',
        data:_tickets
    });
}