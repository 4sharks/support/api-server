import express from 'express';
import {getAllTickets,getByTenant,deleteByTenant,getById,addTicket,createDetails} from '../controllers/ticketController.js';

const router = express.Router();

router.post('/',addTicket);
router.post('/details',createDetails);
router.get('/',getAllTickets);
router.get('/:id',getById);
router.get('/tenant/:tenantId/:companyId/:branchId',getByTenant);
router.delete('/tenant/:tenantId/:companyId/:branchId',deleteByTenant);

export default router;