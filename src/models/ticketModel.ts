import mongoose from "mongoose";

const ticketSchema = new mongoose.Schema({
    customerName:{
        type: String,
        required: [true,"Missing customer name required"]
    },
    customerId:String,
    email:{
        type: String,
        required: [true,"Missing customer name required"]
    },
    mobile:{
        type: String,
        required: [true,"Missing customer name required"]
    },
    subject:{
        type: String,
        required: [true,"Missing customer name required"]
    },
    content:{
        type: String,
        required: [true,"Missing customer name required"]
    },
    sectionId:{
        type: String,
    },
    ProductId:{
        type: String,
    },
    status:{
        type: String,
        default:'NEW'
    },
    attachments: String,
    createdBy: String,
    tenantId:  String,
    companyId: String,
    branchId:  String,
    details:[
        {
            content:{
                type: String,
                required: [true,"Missing customer name required"]
            },
            attachments: String,
            createdBy: String,
            createdAt: Date
        }
    ]

},{
    timestamps: true,
});

const ticketModel =  mongoose.model("Ticket",ticketSchema); 

export default ticketModel;